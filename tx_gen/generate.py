import argparse
import base64
import hashlib
import json
import random
import sys
import time

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.asymmetric import rsa, padding

CERT_DELIM = "-----END CERTIFICATE-----"
PUB_KEYS_DIR = "./publishing_keys/"
PUB_KEYS_LIST = "./publishing_keys/listing.json"

parser = argparse.ArgumentParser(description="Generate transactions")
parser.add_argument('--dns_name', '-d',
                    dest="dns_name",
                    type=str,
                    action="store",
                    help="Defines the DNS for which the tx is being made",
                    required=True)
parser.add_argument('--out', '-o',
                    dest="out",
                    type=argparse.FileType('w'),
                    help="Specify the file where the created tx is stored",
                    required=True)
parser.add_argument('--prev_tx', '-p',
                    dest="prev_tx",
                    type=argparse.FileType('r'),
                    help="Specificy the previous tx file for this DNS name")
parser.add_argument('--public_key', '--pub',
                    dest="public_key",
                    type=argparse.FileType('r'),
                    help="Specify the file which stores the DNS name's public publishing key",
                    required=True)
parser.add_argument('--private_key', '--priv',
                    dest="private_key",
                    type=argparse.FileType('r'),
                    help="Specify the file which stores the DNS name's private publishing key",
                    required=True)
cert_type_group = parser.add_mutually_exclusive_group()
cert_type_group.add_argument('--cert_chain',
                    dest="cert_chain",
                    type=argparse.FileType('r'),
                    help="Use the flag to specify the cert chain file for new certificate tx")
cert_type_group.add_argument('--revoke',
                    dest="revoke",
                    action="store_true",
                    help="Use the flag for revocation cert txs")
cert_type_group.add_argument('--cert',
                    dest="cert",
                    action="store_true",
                    help="Use this flag for update certificate txs")
cert_type_group.add_argument('--publish',
                    dest="publishing",
                    action="store_true",
                    help="Use this flag for publishing txs")

args = parser.parse_args()

def main():
    result = {
        "data": {
            "dns_name": args.dns_name,
            "prev_tx_hash": "",
            "type": 0,
            "cert_list": [],
            "cert_tx_validity": 0,
            "publish_key": []
        },
        "owner_sign": ""
    }

    # Handling the previous tx data
    if args.prev_tx:
        tmp = json.load(args.prev_tx)

        # Determining the hash
        hasher = hashlib.sha256()
        hasher.update(json.dumps(tmp))
        result["data"]["prev_tx_hash"] = hasher.hexdigest();

        # Determing the cert_list
        result["data"]["cert_list"] = tmp["data"]["cert_list"]

        # Clearing the memory
        tmp = None

    # Assigning the type
    if not args.cert_chain and not args.revoke and not args.cert:
        result["data"]["type"] = 2  # Publishing tx
    else:
        result["data"]["type"] = 1  # Certificate tx

    # Assigning validity period
    if result["data"]["cert_list"]:
        valid = 60 * 60 * 24 * 10   # seconds, minutes, hours, days
        result["data"]["cert_tx_validity"] = int(time.time()) + valid

    # Parsing the publishing key
    result["data"]["publish_key"] = args.public_key.read().split('\n')

    # Determining what other processing needs to happen
    if args.cert_chain:
        # Reading and dumping the certs into the output structure
        tmp = args.cert_chain.read()
        result["data"]["cert_chain"] = [(x + CERT_DELIM).strip() for x in tmp.split(CERT_DELIM) if (x and x != "\n")]
        result["data"]["cert_chain"][:] = [x.split("\n") for x in result["data"]["cert_chain"] if (x and x != CERT_DELIM)]

        # Adding the first (real cert) to the cert_list
        tmp = result["data"]["cert_chain"][0]
        tmp = ("\n").join(tmp)
        cert = x509.load_pem_x509_certificate(tmp, default_backend())
        result["data"]["cert_list"].append(base64.b16encode(cert.extensions.get_extension_for_class(x509.SubjectKeyIdentifier).value.digest))

        tmp = None

        # Making sure the cert_tx_validity is set
        if result["data"]["cert_tx_validity"] == 0:
            valid = 60 * 60 * 24 * 10   # seconds, minutes, hours, days
            result["data"]["cert_tx_validity"] = int(time.time()) + valid
    elif args.revoke:
        #TODO
        print "idk how to do this yet"
        sys.exit(0)
    elif args.cert:
        # We actually don't have to do anything here...
        # The fields are already filled out
        pass
    elif args.publishing:
        # Reading the listing file
        with open(PUB_KEYS_LIST, "r") as f:
            publish_list = json.load(f)

        # Printing out the selections
        print "\n"
        print "0\tRandomly choose the rest"
        for i, item in enumerate(publish_list, 1):
            print "{}\t{}".format(str(i), item["name"])

        # Asking for 5 selections
        indexes = []
        while len(indexes) < 5:
            user = int(input("Select the signing entities: "))
            if user < 0 or user > len(publish_list):
                print("Invalid number")
                continue

            # Randomly fill the rest of the array
            if user == 0:
                while len(indexes) < 5:
                    tmp = random.randint(1, len(publish_list))
                    if tmp in indexes:
                        continue
                    else:
                        indexes.append(tmp)
                break
            else:
                if user in indexes:
                    print "Index was already selected"
                else:
                    indexes.append(user)

        # Making the indexes make sense within the context of publish_list
        indexes[:] = [x - 1 for x in indexes]

        # Fill in the DNS names for the selection
        result["data"]["certifier_group"] = [publish_list[x]["name"] for x in indexes]

        # Creating the JSON object in result
        result["list_of_sign"] = {}

        # Making signatures for everyone!
        for index in indexes:
            entity = publish_list[index]
            entity_path = PUB_KEYS_DIR + entity["file"]
            with open(entity_path, "rb") as key_file:
                key = serialization.load_pem_private_key(
                    key_file.read(),
                    password="qwe123",
                    backend=default_backend()
                )
            sig = key.sign(json.dumps(result["data"]), padding.PKCS1v15(), hashes.SHA256())
            result["list_of_sign"][entity["name"]] = base64.b64encode(sig)
    else:
        print "No transaction type specified"
        sys.exit(1)

    # Having the owner sign the transaction
    priv_key = serialization.load_pem_private_key(
        args.private_key.read(),
        password="qwe123",
        backend=default_backend()
    )

    signature = priv_key.sign(json.dumps(result["data"]), padding.PKCS1v15(), hashes.SHA256())
    result["owner_sign"] = base64.b64encode(signature)

    # Writing the results to the given output file
    args.out.write(json.dumps(result))

if __name__ == "__main__":
    main();
