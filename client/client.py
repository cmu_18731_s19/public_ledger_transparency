#!/usr/bin/env python

import argparse
import base64
import hashlib
import json
import merkletools
import sys
import time

from cryptography import x509
from cryptography.hazmat.backends import default_backend

parser = argparse.ArgumentParser(description="Client to test verification times")
parser.add_argument('--cert', '-c',
                    dest="cert_file",
                    type=argparse.FileType('r'),
                    help="Specify the certificate file being verified",
                    required=True)
parser.add_argument('--trans', '-t',
                    dest="tx_file",
                    type=argparse.FileType('r'),
                    help="Specify the transaction file being verified",
                    required=True)
parser.add_argument('--head', '-d',
                    dest="header_file",
                    type=argparse.FileType('r'),
                    help="Specify the client block headers file",
                    required=True)
parser.add_argument('--audit', '-a',
                    dest="audit_file",
                    type=argparse.FileType('r'),
                    help="Specify the audit path file",
                    required=True)
parser.add_argument('--runs', '-r',
                    dest="runs",
                    type=int,
                    action="store",
                    help="Specify the number of runs to average across",
                    default=3)
parser.add_argument('--verbose', '-v',
                    dest="verbose",
                    action="store_true",
                    help="(not implemented) Using this flag will print data from all runs")

args = parser.parse_args()

def main():
    mt = merkletools.MerkleTools(hash_type='sha256')

    # Reading the cert file and grabbing some data
    cert = x509.load_pem_x509_certificate(args.cert_file.read(), default_backend())
    cert_id = base64.b16encode(cert.extensions.get_extension_for_class(x509.SubjectKeyIdentifier).value.digest)
    dns = ""
    for x in cert.subject.rfc4514_string().split(","):
        if "CN=" in x:
            dns = x[3:]

    # Reading the audit file and finding the correct one
    audit = json.loads(args.audit_file.read())
    audit_path = audit[dns][cert_id]["path"]
    block_id = audit[dns][cert_id]["id"]

    # Reading the transaction file
    tx_string = args.tx_file.read()

    # Preping for looping
    hasher = hashlib.sha256()
    verified = False
    start_time, end_time = 0, 0
    timings = {
        "cert_tx": [],
        "read_headers": [],
        "merkle_check": []
    }

    for _ in range(args.runs):
        # TIME: verify certificate is in the transaction and transaction is still valid
        start_time = time.time()
        tx = json.loads(tx_string)
        if cert_id not in tx["data"]["cert_list"]:
            print("Certificate is not in the given transaction!")
            break
        if tx["data"]["cert_tx_validity"] - int(time.time()) <= 0:
            print("Certificate transaction is not longer valid!")
            break
        end_time = time.time()

        timings["cert_tx"].append(end_time - start_time)

        # TIME: Read headers from file, find block we want, check all other blocks
        start_time = time.time()
        block = None
        found = False
        headers = json.loads(args.header_file.read())
        for item in headers["verified"]:
            if found:
                # Checking blocks for the dns name
                if dns in item["dns"].keys():
                    print("Failing verification due to another verified block containing the dns name")
                    found = False
                    break
            else:
                # Case where we are still searching for the block
                if block_id == item["id"]:
                    block = item
                    found = True
                elif item["id"] > block_id:
                    print("Could not find a verified block with the given block id")
                    break

        if not found:
            break

        # Checking the unverified blocks
        for item in headers["unverified"]:
            if dns in item["dns"].keys():
                print("Failing verification due to another unverified block containing the dns name")
                found = False
                break

        if not found:
            break

        end_time = time.time()
        timings["read_headers"].append(end_time - start_time)

        # TIME: Verify Merkle Root
        start_time = time.time()
        hasher.update(tx_string.encode('utf-8'))
        print(audit_path)
        print(hasher.hexdigest())
        print(block["merkle"])
        if not mt.validate_proof(audit_path,
                hasher.hexdigest(),
                block["merkle"]):
            print("Merkle tree failed to verify")
            break

        end_time = time.time()
        timings["merkle_check"].append(end_time - start_time)

        # Cleanup for next loop
        args.header_file.seek(0)
        verified = True
        hasher = hashlib.sha256()

    # Calculating the averages of the
    timings["total"] = []
    for i in range(args.runs):
        tot = timings["cert_tx"][i] + timings["read_headers"][i] + timings["merkle_check"][i]
        timings["total"].append(tot)

    for key in timings:
        if len(timings[key]) == 0:
            tmp = 0
        else:
            tmp = sum(timings[key]) / len(timings[key])
        timings[key].append("avg: {}".format(tmp))

    # Output the results
    print("*"*50)
    print("Timing for checking transaction:")
    print(timings["cert_tx"])

    print("")
    print("Timing for reading and parsing the header file:")
    print(timings["read_headers"])

    print("")
    print("Timing for checking the Merkle tree:")
    print(timings["merkle_check"])

    print("")
    print("Totals:")
    print(timings["total"])

if __name__ == "__main__":
    main()
