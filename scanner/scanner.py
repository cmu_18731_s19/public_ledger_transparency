#!/usr/bin/env python3

import argparse
import base64
import json
import merkletools
import sys

from ethereum import utils
from ethereum.tools import tester

parser = argparse.ArgumentParser(description="Build a test chain and corresponding output files")
parser.add_argument('--in', '-i',
                    dest="listing_file",
                    type=argparse.FileType('r'),
                    help="Specify the listing file for test chain to build",
                    required=True)
parser.add_argument('--out', '--client', '-o', '-c',
                    dest="client_out",
                    type=argparse.FileType('w'),
                    help="Output file that the client will read (block headers)",
                    required=True)
parser.add_argument('--audit', '-a',
                    dest="audit_out",
                    type=argparse.FileType('w'),
                    help="Output file where audit paths will be stored",
                    required=True)

args = parser.parse_args()

def main():
    # Setting up main data structure for parsing the files
    mt = merkletools.MerkleTools(hash_type='sha256')
    listings = json.loads(args.listing_file.read())
    bc = tester.Chain()

    # Initializing the data structure for data output
    output = []
    audit = {}
    cur_block = {"dns":{}}

    # Start looping through the listing files
    for item in listings:
        if item == "MINE_BLOCK":
            # TODO remove duplicate code
            bc.mine()

            # Updating all of the data structures
            mt.make_tree()
            cur_block["merkle"] = mt.get_merkle_root()
            for name in cur_block["dns"].keys():
                for id in audit[name].keys():
                    print(mt.is_ready)
                    print(audit[name][id]["id"])
                    print(mt.get_proof(audit[name][id]["id"]))
                    audit[name][id]["path"] = mt.get_proof(audit[name][id]["id"])
                    audit[name][id]["id"] = bc.chain.get_parent(bc.block).header.number
                    # audit[name][id].pop("id", None)

            # TODO include full block header + our stuff
            cur_block["id"] = bc.chain.get_parent(bc.block).header.number
            cur_block["time"] = bc.chain.get_parent(bc.block).header.timestamp
            cur_block["prevhash"] = base64.b64encode(bc.chain.get_parent(bc.block).header.prevhash).decode('utf-8')
            cur_block["hash"] = base64.b64encode(bc.chain.get_parent(bc.block).header.hash).decode('utf-8')
            cur_block["mixhash"] = base64.b64encode(bc.chain.get_parent(bc.block).header.mixhash).decode('utf-8')
            cur_block["nonce"] = base64.b64encode(bc.chain.get_parent(bc.block).header.nonce).decode('utf-8')

            output.append(cur_block)
            cur_block = {"dns":{}}
            mt.reset_tree()
        else:
            # opening and loading the transaction
            with open(item, 'rb') as f:
                current = f.read().decode('utf-8')
            current_json = json.loads(current)

            # Grabbing some data we need for the output
            cur_name = current_json["data"]["dns_name"]
            cur_type = current_json["data"]["type"]
            cur_certs = current_json["data"]["cert_list"]

            # Initializing audit paths
            if cur_name not in audit.keys():
                audit[cur_name] = {}
            for key_id in cur_certs:
                audit[cur_name][key_id] = {"id": mt.get_leaf_count()}

            # Creating the transaction
            bc.tx(tester.k1, tester.a0, 1000, bytes(current, 'utf-8'))
            mt.add_leaf(current, True)
            print(mt.get_leaf_count())
            print(mt.get_leaf(0))

            # Updating the cur block
            if cur_name in cur_block["dns"]:
                print("Failure on the following line: " + item)
                print("Cannot have two transactions in a block from the same dns name!")
                sys.exit(1)
            cur_block["dns"][cur_name] = cur_type

            # Checking if the block needs to be mined
            if bc.head_state.gas_limit - bc.head_state.gas_used < 20000:
                bc.mine()

                # Updating all of the data structures
                mt.make_tree()
                cur_block["merkle"] = mt.get_merkle_root()
                for name in cur_block["dns"].keys():
                    for id in audit[name].keys():
                        print(audit[name][id]["id"])
                        audit[name][id]["path"] = mt.get_proof(audit[name][id]["id"])
                        audit[name][id]["id"] = bc.chain.get_parent(bc.block).header.number
                        # audit[name][id].pop("id", None)

                cur_block["id"] = bc.chain.get_parent(bc.block).header.number
                cur_block["time"] = bc.chain.get_parent(bc.block).header.timestamp
                cur_block["prevhash"] = base64.b64encode(bc.chain.get_parent(bc.block).header.prevhash).decode('utf-8')
                cur_block["hash"] = base64.b64encode(bc.chain.get_parent(bc.block).header.hash).decode('utf-8')
                cur_block["mixhash"] = base64.b64encode(bc.chain.get_parent(bc.block).header.mixhash).decode('utf-8')
                cur_block["nonce"] = base64.b64encode(bc.chain.get_parent(bc.block).header.nonce).decode('utf-8')

                output.append(cur_block)
                cur_block = {"dns":{}}
                mt.reset_tree()

    # Reparsing the output dict to format the client is expecting
    output = {
        "verified": output[:-6],
        "unverified": output[-6:]
    }

    # Writing the data to files
    args.client_out.write(json.dumps(output))
    args.audit_out.write(json.dumps(audit))

if __name__ == "__main__":
    main()
